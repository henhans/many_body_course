import matplotlib.pyplot as plt
import numpy as np
from scipy.linalg import eigh
np.set_printoptions(suppress=True)

Nsite = 100
tmat = np.zeros((Nsite,Nsite),dtype=complex)
t = 1.0

for i in range(Nsite):
    if i < Nsite-1: 
        tmat[i,i+1] = -t
    if i > 0:
        tmat[i,i-1] = -t
tmat[0,Nsite-1] = -t
tmat[Nsite-1,0] = -t

print(tmat)

evals, evecs = eigh(tmat)

print(evals)

ks = -np.pi+np.arange(Nsite)*2*np.pi/Nsite
#plt.plot(evals,'o-')
#plt.show()

urk = np.zeros((Nsite,Nsite),dtype=complex)
for i in range(Nsite):
    for j in range(Nsite):
        urk[i,j] = np.exp(-1j*i*ks[j])/np.sqrt(Nsite)

assert(np.allclose(np.dot(urk.conj().T,urk), np.eye(urk.shape[0])))
hk = urk.conj().T.dot(tmat).dot(urk)
print(hk)
ek = hk.diagonal().real

plt.plot(ks, ek, '-o')
plt.plot(ks,-2*t*np.cos(ks))
plt.show()

#print(u.dot(tmat).dot(u.conj().T).diagonal())

Nom = 200
oms = np.linspace(-2.5,2.5,Nom)
delta = 0.05
Doms = np.zeros((Nom),dtype=float)

for iom, om in enumerate(oms):
    sumk = 0.0
    for e in ek:
        sumk += ( 1./(om+1j*delta-e) ).imag
    sumk = -sumk/(Nsite*np.pi)
    Doms[iom] = sumk

plt.plot(oms, Doms)
plt.show()
