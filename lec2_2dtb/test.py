from lattice import *

#argument(dimension, unit vector, size of lattice, number of atoms in the unit cel, atom's coordinate)
lat = lattice(2,[[1,0,0],[0,1,0],[0,0,0]],[40,40,1],1,[[0,0,0]])
lat.plot_lattice()

t = -1.0
hop = {}
#             (0,1,0)
#                |
#   (-1,0,0)--(0,0,0)--(1,0,0)
#                |
#             (0,-1,0)
#  [original atom index at coordinate (0,0,0)]       [hop to atom index, coordinate x, y, z, hopping amplitude  
hop[0] = [[0,1,0,0,t],
          [0,-1,0,0,t],
          [0,0,1,0,t],
          [0,0,-1,0,t]]
hop_mat = lat.build_mat_TB_PBC(hop,antiperiodic=False)
#evals, evecs = eigh(hop_mat)

hop_k = lat.FT_hop_ij()
print(hop_k.real)
#tmp = np.zeros(hop_k.shape,dtype=np.complex)
#tmp[range(hop_k.shape[0]),range(hop_k.shape[1])] = hop_k.diagonal()
#assert np.allclose(hop_k-tmp, np.zeros(hop_k.shape,dtype=np.complex))

evals = hop_k.diagonal()
#print evals

#test non-interacting DOS
oms = np.linspace(-4.2,4.2,100)
Gk0 = np.array([[1./((om+1j*0.05)-e) for om in oms] for e in evals])
G0 = np.sum(Gk0, axis=0)/len(evals)
import matplotlib.pyplot as plt
plt.plot(oms, -G0[:].imag)
plt.show()

