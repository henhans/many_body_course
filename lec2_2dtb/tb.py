from lattice import *
#from numba import jit

def build_HGraphene(Nx,Ny,mu=0):
  '''
  Build tight-binding model to graphene only. 
  '''
  N = Nx*Ny*2
  t = 1.
  print("N=",N)
  mat_TB = np.zeros((N,N),dtype=complex)
  mat_TB += (mu)*np.eye(N,dtype=complex)
  
  for ix in range(Nx):
    for iy in range(Ny):
      for dx in [-1,0,1]:
        for dy in [-1,0,1]:
          jx = ix + dx
          
          if jx >= Nx: jx += -Nx
          #jx += ifelse(jx > Nx,-Nx,0)
          if jx < 0:  jx += Nx
          #jx += ifelse(jx < 1,Nx,0)
          
          jy = iy + dy
          if jy >= Ny: jy += -Ny
          #jy += ifelse(jy > Ny,-Ny,0)
          if jy < 0: jy += Ny
          #jy += ifelse(jy < 1,Ny,0)

          for a in [1,2]:
            if a == 1: b=2
            else: b=1
            #b = ifelse(a ==1,2,1)
            ii = ((iy-1)*Nx+ix-1)*2+a
            jj = ((jy-1)*Nx+jx-1)*2+b

            print(ix, iy, jx, jy, ii, jj)
            if dx == 0 and dy == 0:
                mat_TB[ii,jj] = t
            elif dx == +1 and dy==0 and a ==1:
                mat_TB[ii,jj] = t
            elif dx == 0 and dy == 1 and a ==1:
                mat_TB[ii,jj] = t
            elif dx == -1 and dy ==0 and a ==2:
                mat_TB[ii,jj] = t
            elif dx ==0 and dy == -1 and a ==2:
                mat_TB[ii,jj] = t  
  return mat_TB

def build_mat_TB_OBC(Nx,Ny,Nz,Natom,hops,mu=0):
  '''
  Build general TB model
  matrix ordered as 
  (0,0,0) (1,0,0) ... (nx,0,0) (nx,1,0) ... (nx,ny,0) (nx,ny,1) ... (nx,ny,nz)
  each () block contain number Natom of atoms
  '''
  N = Nx*Ny*Nz*Natom
  print("N=", N)
  mat_TB = np.zeros((N,N),dtype=complex)
  mat_TB += (mu)*np.eye(N,dtype=complex)

  for i in range(Nx): 
    for j in range(Ny): 
      for k in range(Nz): 
        for a in range(Natom):
          #print i, j, k, a, i*Ny*Nz*Natom + j*Nz*Natom + k*Natom + a
          # lookup index
          ii = i*Ny*Nz*Natom + j*Nz*Natom + k*Natom + a
#          for hop in hops[a]:
          for h in range(len(hops[a])):
            hop = hops[a][h]
            if i+hop[1]<Nx and i+hop[1]>=0 and j+hop[2]<Ny and j+hop[2]>=0 and k+hop[3]<Nz and k+hop[3]>=0:
              jj = (i+hop[1])*Ny*Nz*Natom + (j+hop[2])*Nz*Natom + (k+hop[3])*Natom + hop[0]
              mat_TB[ii,jj] += hop[4]

  return mat_TB



def build_mat_TB_PBC(Nx,Ny,Nz,Natom,hops,mu=0):
  '''
  Build general TB model
  matrix ordered as 
  (0,0,0) (1,0,0) ... (nx,0,0) (nx,1,0) ... (nx,ny,0) (nx,ny,1) ... (nx,ny,nz)
  each () block contain number Natom of atoms
  '''
  N = Nx*Ny*Nz*Natom
  print(N)
  mat_TB = np.zeros((N,N),dtype=complex)
  mat_TB += (mu)*np.eye(N,dtype=complex)

  for i in range(Nx): 
    for j in range(Ny): 
      for k in range(Nz): 
        for a in range(Natom):
          #print i, j, k, a, i*Ny*Nz*Natom + j*Nz*Natom + k*Natom + a
          # lookup index
          ii = i*Ny*Nz*Natom + j*Nz*Natom + k*Natom + a
          for hop in hops[a]:
#          for h in range(len(hops[a])):
#            hop = hops[a][h]
            jj = (i+hop[1])%Nx*Ny*Nz*Natom + (j+hop[2])%Ny*Nz*Natom + (k+hop[3])%Nz*Natom + hop[0]
            mat_TB[ii,jj] += hop[4]

  return mat_TB

def build_mat_TB_PBCY(Nx,Ny,Nz,Natom,hops,mu=0):
  '''
  Build general TB model
  matrix ordered as 
  (0,0,0) (1,0,0) ... (nx,0,0) (nx,1,0) ... (nx,ny,0) (nx,ny,1) ... (nx,ny,nz)
  each () block contain number Natom of atoms
  '''
  N = Nx*Ny*Nz*Natom
  print(N)
  mat_TB = np.zeros((N,N),dtype=complex)
  mat_TB += (mu)*np.eye(N,dtype=complex)

  for i in range(Nx): 
    for j in range(Ny): 
      for k in range(Nz): 
        for a in range(Natom):
          #print i, j, k, a, i*Ny*Nz*Natom + j*Nz*Natom + k*Natom + a
          # lookup index
          ii = i*Ny*Nz*Natom + j*Nz*Natom + k*Natom + a
          for hop in hops[a]:
#          for h in range(len(hops[a])):
#            hop = hops[a][h]
            if i+hop[1]<Nx and i+hop[1]>=0 and k+hop[3]<Nz and k+hop[3]>=0:
              jj = (i+hop[1])*Ny*Nz*Natom + (j+hop[2])%Ny*Nz*Natom + (k+hop[3])*Natom + hop[0]
              mat_TB[ii,jj] += hop[4]

  return mat_TB

def build_mat_TB_PBCX(Nx,Ny,Nz,Natom,hops,mu=0):
  '''
  Build general TB model
  matrix ordered as 
  (0,0,0) (1,0,0) ... (nx,0,0) (nx,1,0) ... (nx,ny,0) (nx,ny,1) ... (nx,ny,nz)
  each () block contain number Natom of atoms
  '''
  N = Nx*Ny*Nz*Natom
  print(N)
  mat_TB = np.zeros((N,N),dtype=complex)
  mat_TB += (mu)*np.eye(N,dtype=complex)

  for i in range(Nx): 
    for j in range(Ny): 
      for k in range(Nz): 
        for a in range(Natom):
          #print i, j, k, a, i*Ny*Nz*Natom + j*Nz*Natom + k*Natom + a
          # lookup index
          ii = i*Ny*Nz*Natom + j*Nz*Natom + k*Natom + a
          for hop in hops[a]:
#          for h in range(len(hops[a])):
#            hop = hops[a][h]
            if j+hop[2]<Ny and j+hop[2]>=0 and k+hop[3]<Nz and k+hop[3]>=0:
              jj = (i+hop[1])%Nx*Ny*Nz*Natom + (j+hop[2])*Nz*Natom + (k+hop[3])*Natom + hop[0]
              mat_TB[ii,jj] += hop[4]

  return mat_TB


def fourier_y_ky(ky,Nx,Ny,Nz,Natom,mat_TB):
  '''
  Fourier transform y to ky
  ''' 
  mat = np.zeros((Nx*Nz*Natom,Nx*Nz*Natom),dtype=complex)
  #print mat.shape
  V = np.zeros((Nx*Nz*Natom,Nx*Ny*Nz*Natom),dtype=complex)
  for i in range(Nx): 
    for j in range(Ny): 
      for k in range(Nz): 
        for a in range(Natom):
          #print i, j, k, a, i*Ny*Nz*Natom + j*Nz*Natom + k*Natom + a
          # lookup index
          ii = i*Nz*Natom + k*Natom + a
          jj = i*Ny*Nz*Natom + j*Nz*Natom + k*Natom + a
          V[ii,jj] = np.exp(1j*ky*j)
  V/=np.sqrt(Ny)
  #V = [np.exp(1j*ky*i) for i in range(Ny)]
  #print V
  mat = np.dot(V,np.dot(mat_TB,V.conj().T))
  return mat 

if __name__ == '__main__':

  # triangular
  #lat = lattice(2,[[1,0,0],[0.5,1,0],[0,0,0]],[10,10,0],1,[[0,0,0]])
  #lat.print_lattice_info()
  #lat.plot_lattice()

  # honeycomb
  #lat = lattice(2,[[1,0,0],[-0.5,np.sqrt(3)/2.,0],[0,0,0.]],[50,20,1],2,[[0,0,0],[0,1./np.sqrt(3),0]])
  #lat = lattice(2,[[0,1,0],[np.sqrt(3)/2.,-0.5,0],[0,0,0.]],[50,50,1],2,[[0,0,0],[1./np.sqrt(3),0,0]])
  #zigzag
  lat = lattice(2,[[0,np.sqrt(3),0],[3./2.,np.sqrt(3)/2,0],[0,0,0.]],[20,20,1],2,[[0,0,0],[0.5,np.sqrt(3)/2,0]])
  #lat = lattice(2,[[0,np.sqrt(3),0],[3./2.,np.sqrt(3)/2,0],[0,0,0.]],[3,3,1],2,[[0,0,0],[0.5,np.sqrt(3)/2,0]])
  #lat.print_lattice_info()
  #lat.plot_lattice()


  t1 = -1
  t2 = 0.053
  phi = np.pi/2.
  m = 0.2
  #print t2*np.exp(1j*phi)
  # Test hopping dictionary matrix
  hops = {} #key: atom0, element: hopping matrix [atom1,dx,dy,dz,t]
  hops[0] = [[1,0,0,0,t1],
             [1,-1,0,0,t1],
             [1,0,-1,0,t1],
             [0,0,0,0,m],
             [0,1,0,0,t2*np.exp(1j*phi)],
             [0,0,1,0,t2*np.exp(-1j*phi)],
             [0,-1,1,0,t2*np.exp(1j*phi)],
             [0,-1,0,0,t2*np.exp(-1j*phi)],
             [0,0,-1,0,t2*np.exp(1j*phi)],
             [0,1,-1,0,t2*np.exp(-1j*phi)]]
  hops[1] = [[0,0,0,0,t1],
             [0,1,0,0,t1],
             [0,0,1,0,t1],
             [1,0,0,0,-m],
             [1,1,0,0,t2*np.exp(-1j*phi)],
             [1,0,1,0,t2*np.exp(1j*phi)],
             [1,-1,1,0,t2*np.exp(-1j*phi)],
             [1,-1,0,0,t2*np.exp(1j*phi)],
             [1,0,-1,0,t2*np.exp(-1j*phi)],
             [1,1,-1,0,t2*np.exp(1j*phi)]]
  # list atom1, dx, dy, dz, t
  #hops = []
  #atom 1
  #hops.append([[1,0,0,0,-1],[1,-1,0,0,-1],[1,0,-1,0,-1]])
  #atom 2
  #hops.append([[0,0,0,0,-1],[0,1,0,0,-1],[0,0,1,0,-1]])
  #hops= np.array(hops)

#  mat_TB = build_HGraphene(10,50)
#  mat_TB = build_mat_TB_OBC(lat,hops)
#  mat_TB = build_mat_TB_PBC(lat.nxyz[0],lat.nxyz[1],lat.nxyz[2],lat.natom,hops)
  mat_TB = build_mat_TB_PBCY(lat.nxyz[0],lat.nxyz[1],lat.nxyz[2],lat.natom,hops)
#  mat_TB = build_mat_TB_PBCX(lat.nxyz[0],lat.nxyz[1],lat.nxyz[2],lat.natom,hops)
  #print mat_TB

  import matplotlib.pyplot as plt
  from scipy.linalg import eigh
  energy,mat_v = eigh(mat_TB)

  #print np.sort(energy)[len(energy)/2-100:len(energy)/2+100]
  #plt.hist(energy,bins=100)
  #plt.show()

  #oms = np.linspace(-4,4,200)
  #dos = [np.sum([-1./np.pi*(1./(om-e+1j*0.03)).imag for e in energy])/len(energy) for om in oms ]
  #plt.plot(oms,dos)
  #plt.xlabel('$\omega$')
  #plt.ylabel('$DOS$')
  #plt.show()

  # plot Fourier
  nky = lat.nxyz[1]
  vky = np.linspace(-np.pi,np.pi,nky)
  ep = np.zeros((nky,lat.nxyz[0]*lat.nxyz[2]*lat.natom))
  cnt = 0
  for ky in vky:
    mat = fourier_y_ky(ky,lat.nxyz[0],lat.nxyz[1],lat.nxyz[2],lat.natom,mat_TB)
    energy,mat_v = eigh(mat)
    for i in range(lat.nxyz[0]*lat.nxyz[2]*lat.natom):
        ep[cnt,i] = energy[i]
    cnt+=1
  plt.plot(vky, ep,'-')
  plt.xlim(-np.pi,np.pi)
  plt.xlabel('$k_y$')
  plt.ylabel('$E$')
  plt.show()
