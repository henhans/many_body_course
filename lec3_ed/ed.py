import numpy as np
from scipy.sparse import csr_matrix, identity
from scipy.sparse.linalg import eigsh
from itertools import product

def build_fermion_op(no):
    '''
    Build fermionic operators for each spin+orbitals, alpha, with size 
    of Hilberspace, A and B.
    Input:
        no: number of orbital
    Output:
        FH_list: a list of fermionic operator <A|[f^dagger_alpha]|B>
    '''
    
    hsize = 2**no # size of Hilbert space
    strb = '{0:0'+str(no)+'b}' # string to save binary configuration
    FH_list = []
    #print 'Hibert space size:', hsize
    for o in range(no): #calculate FH for each orbital
        #print 'building FH for orbital:', o
        row = [] #storing row index for 
        col = [] #storing col index for
        data = [] #storing data for (row, col)
        for b in range(hsize): # col |B>
            config_b = strb.format(b) # cofiguration correspond to B
            #print config_b #print binary configuration
            #print config_b[5]
            if config_b[o] == '0':
                col.append(b)
                config_a = ''
                #calculate the corresponding <A|
                for i in range(no):
                    if i == o:
                        config_a += '1'
                    else:
                        config_a += config_b[i]
                a = int(config_a,2)
                row.append(a)
                #calculate the exponent for minus sign
                expo = 0
                for i in range(o):
                    expo += int(config_b[i])
                data.append((-1.)**(expo))
                #print config_a
                #print config_b
                #print (-1.)**(expo), expo
        #print '<|=',row
        #print '|>=',col
        #print 'data=',data
        #assign values into sparse matrix
        FH = csr_matrix((data, (row, col)), shape=(hsize, hsize), dtype=float)
        #print FH.toarray()
        FH_list.append(FH)
    return FH_list

def operators_to_eigenbasis(op_vec, U):
    dop_vec = []        
    for op in op_vec:
        dop = np.mat(U).H * op * np.mat(U)
        dop_vec.append(dop) 

    return dop_vec
                    
def compute_Gii_thermal(ii, T, oms, evals, evecs, FH_list_dense):
    # -- Components of the Lehman expression
    dE = - evals[:, None] + evals[None, :]
    #exp_bE = np.exp(- evals/T)
    exp_bE = np.zeros(evals.shape)
    for ie,e in enumerate(evals):
        if e/T > 500.:
            exp_bE[ie] = np.exp(-500.)
        elif e/T < -500.:
            exp_bE[ie] = np.exp(500.)
        else:
            exp_bE[ie] = np.exp(-e/T)
    M = exp_bE[:, None] + exp_bE[None, :]

    inv_freq = oms[:, None, None] - dE[None, :, :]
    nonzero_idx = np.nonzero(inv_freq)
    # -- Only eval for non-zero values
    freq = np.zeros_like(inv_freq)
    freq[nonzero_idx] = inv_freq[nonzero_idx]**(-1)

    op1_eig, op2_eig = operators_to_eigenbasis([FH_list_dense[ii].conj().T, FH_list_dense[ii]], evecs)

    # -- Compute Lehman sum for all operator combinations
    Gw = np.zeros((len(oms)), dtype=complex)
    Gw = np.einsum('nm,mn,nm,znm->z', op1_eig, op2_eig, M, freq)
    Gw /= np.sum(exp_bE)
    return Gw

