from ed import *
from scipy.linalg import eigh
import matplotlib.pyplot as plt

Nsite = 2
Norb = 2*Nsite # spin times sites

FH_list = build_fermion_op(Norb)

for U in [0.0, 3.0, 6.0, 9.0]:
    t = 1.0
    mu = -U/2.
    
    H = -t*( FH_list[0].dot(FH_list[2].conj().T) + FH_list[2].dot(FH_list[0].conj().T)
           + FH_list[1].dot(FH_list[3].conj().T) + FH_list[3].dot(FH_list[1].conj().T) )
    H += mu*( FH_list[0].dot(FH_list[0].conj().T) + FH_list[1].dot(FH_list[1].conj().T)
            + FH_list[2].dot(FH_list[2].conj().T) + FH_list[3].dot(FH_list[3].conj().T) )
    H += U*FH_list[0].dot(FH_list[0].conj().T).dot(FH_list[1]).dot(FH_list[1].conj().T)
    H += U*FH_list[2].dot(FH_list[2].conj().T).dot(FH_list[3]).dot(FH_list[3].conj().T)
    
    # solve eigen energies
    evals, evecs = eigh(H.todense())
    
    print('Energy levels=', evals)
    
    # solve ground state occupancy for all the orbitals 
    
    for i in range(Norb):
        ni = FH_list[i].dot(FH_list[i].conj().T)
        ni_avg = evecs[:,0].conj().T.dot( ni.todense() ).dot(evecs[:,0])
        print('n%d_avg='%(i), ni_avg)
    
    # compute Green's function for the 0up orbital
    
    T = 0.05 # temperature
    eta = 0.1 # broadening factor
    oms = np.linspace(-10,10,500) + 1j*eta # frequency with broadening
    FH_list_dense = [FH_list[i].todense() for i in range(Norb)] # operators in dense matrix
    
    Goms = compute_Gii_thermal(0, T, oms, evals, evecs, FH_list_dense)
    
    plt.plot(oms, -Goms.imag/np.pi,'-', label='U=%.2f'%(U))

plt.xlabel('$\omega$')
plt.ylabel('DOS($\omega$)')
plt.legend(loc='best')
plt.show()
