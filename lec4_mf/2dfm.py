from lattice import *

def nf(beta,x):
    '''
    Fermi function
    '''
    if abs(beta*x) <= 500:
        return 1./(np.exp(beta*x)+1).real
    elif beta*x > 500.:
        return 0.0
    elif beta*x < -500.:
        return 1.0

def compute_nup(eks,U,ndn,mu,beta,field):
    '''
    field is a small magnetic field h=B*Sz, Sz=+1/2 for spin up and Sz=-1/2 for spin down
    Notes: len(eks)=Nlatt (number of lattice site)
    '''
    nup = 0.0
    for ik, ek in enumerate(eks):
        nup += nf(beta,(ek+U*ndn+field-mu))
    nup = nup/len(eks)
    return nup

def compute_ndn(eks,U,nup,mu,beta,field):
    '''
    Homework: assign the correct value to ndn (electron filling factor for down spin)
    field is a small magnetic field h=B*Sz, Sz=+1/2 for spin up and Sz=-1/2 for spin down
    '''
    ndn = 0.0
    for ik, ek in enumerate(eks):
        ndn += 1234 #assign the correct value to ndn here
    ndn = ndn/len(eks)
    return ndn

def func_mu(x, *args):
    '''
    Homework: assign the correct value to ntot (total electron filling factor)
    '''
    #print('x=',x)
    eks, U, nup, ndn, beta, field = args
    ntot = 0.0
    for ik, ek in enumerate(eks):
        ntot += 1234 #assign the correct value to ntot here
    ntot = ntot/len(eks)
    #print('ntot=',ntot)
    return (n - ntot).real

def compute_mu(mu0, eks, U,nup, ndn, beta, field):
    from scipy.optimize import root_scalar 
    sols = root_scalar(func_mu, x0=mu0, x1=mu0+0.1, args=(eks, U, nup, ndn ,beta, field), method='secant', xtol=1e-8) 
    mu = sols.root
    return mu

def compute_dos_up(oms, eta, eks, nup, ndn, U, mu, field):
    dos = np.zeros(oms.shape)
    for iom, om in enumerate(oms):
        sumk =0.0
        for ek in eks:
            hk = ek+U*ndn+field-mu
            sumk += (1./ (om + 1j*eta - hk) ).imag/np.pi
        sumk = sumk/len(eks)
        dos[iom] = sumk
    return dos

def compute_dos_dn(oms, eta, eks, nup, ndn, U, mu, field):
    '''
    Homework: complete the function for computing the density of state for spin down. 
    '''
    dos = np.zeros(oms.shape) # assign the correct density of state here
    for iom, om in enumerate(oms):
        sumk =0.0
        for ek in eks:
            hk = 1234
            sumk += (1./ (om + 1j*eta - hk) ).imag/np.pi
        sumk = sumk/len(eks)
        dos[iom] = sumk
    return dos

#argument(dimension, unit vector, size of lattice, number of atoms in the unit cel, atom's coordinate)
lat = lattice(2,[[1,0,0],[0,1,0],[0,0,0]],[30,30,1],1,[[0,0,0]])
#lat.plot_lattice()

t = -1.0
hop = {}
'''
Homework: assign the correct hopping matrix
'''
hop[0] = [1234] #assign the correct hopping matrix here
hop_mat = lat.build_mat_TB_PBC(hop,antiperiodic=False)
#evals, evecs = eigh(hop_mat)

hop_k = lat.FT_hop_ij()
#print(hop_k.real)
#tmp = np.zeros(hop_k.shape,dtype=np.complex)
#tmp[range(hop_k.shape[0]),range(hop_k.shape[1])] = hop_k.diagonal()
#assert np.allclose(hop_k-tmp, np.zeros(hop_k.shape,dtype=np.complex))

eks = hop_k.diagonal()
#print(eks)

#test non-interacting DOS
#oms = np.linspace(-4.2,4.2,100)
#Gk0 = np.array([[1./((om+1j*0.05)-e) for om in oms] for e in eks])
#G0 = np.sum(Gk0, axis=0)/len(eks)
#import matplotlib.pyplot as plt
#plt.plot(oms, -G0[:].imag)
#plt.show()

beta = 200. # temperature
n = 1.4 # electron filling
field = 0.00 # magnetic field
maxiter = 500 # maximum iteration
tol = 1e-8 # convergence tolerance
mix = 0.5 # mixing factor

U = 16.0 # Coulomb interaction

nup0 = 1.0 # initial guess of nup
ndn0 = 0.5 # inital guess of udn
mu0 = 14.0 # initial chemical potential

print('************************************** Solving U=',U,'***************************************') 
diff = 1e10
for it in range(maxiter):
    ndn_new = compute_ndn(eks,U,nup0,mu0,beta,field)
    nup_new = compute_nup(eks,U,ndn0,mu0,beta,field)
    mu_new = compute_mu(mu0,eks,U,nup_new,ndn_new,beta,field)

    diff = np.abs(mu_new - mu0) + np.abs(ndn_new-ndn0) + np.abs(nup_new-nup0)
    # mixing new and old solutions
    nup0 = mix*nup_new + (1-mix)*nup0
    ndn0 = mix*ndn_new + (1-mix)*ndn0
    mu0 = mix*mu_new + (1-mix)*mu0
    '''
    Homework: assign the correct value to mz
    '''
    mz = 1234 #assign the correct value to mz here

    print('iteration=', it, 'diff=', diff,'ndn=',ndn0, 'nup=', nup0, 'mu=', mu0, 'mz=', mz)
    if diff < tol:
        print('******************************* converged! *********************************')
        print('converged magnetization is:', mz )
        print('****************************************************************************')
        break

oms = np.linspace(-15,10,200)
eta = 0.1

dos_up = compute_dos_up(oms, eta, eks, nup0, ndn0, U, mu0, field)
dos_dn = compute_dos_dn(oms, eta, eks, nup0, ndn0, U, mu0, field)

import matplotlib.pyplot as plt

plt.plot(oms, dos_up, 'b-', label=r'$A_{\uparrow}(\omega)$')
plt.plot(oms, -dos_dn, 'r-', label=r'$A_{\downarrow}(\omega)$')
plt.axvline(0,color='k',lw=1)
plt.xlabel('$\omega$',size=15)
plt.ylabel('$DOS$',size=15)
plt.xticks(size=15)
plt.yticks(size=15)
plt.xlim(-15,10)
plt.legend(loc='best',fontsize=15)
plt.tight_layout()
plt.show()
